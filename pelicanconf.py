AUTHOR = 'Arsalan Sefidgar'
SITENAME = 'Arsalan blog'
SITESUBTITLE = "DevOps / Cloud Engineer"
SITEURL = "https://blog.arsalanse.ir"

PATH = "content"

TIMEZONE = 'Asia/Tehran'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("arsalanse.ir", "https://arsalanse.ir"),
    ("blog.arsalanse.ir", "https://blog.arsalanse.ir"),
)

# Social widget
SOCIAL = (
    ("LinkedIn", "https://www.linkedin.com/in/arsalanse"),
)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {
        'path': 'favicon.ico',
    }
}
